import { NgModule } from '@angular/core';
import { MoviesCatalogComponent } from './movies-catalog/movies-catalog.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { sharedModule } from '../shared/shared.module';
import { CoreRoutingModule } from './core-routing.module';


@NgModule({
  declarations: [
    MoviesCatalogComponent,
    MovieDetailsComponent
    ],
  imports: [
    sharedModule,
    CoreRoutingModule
  ]
})
export class CoreModule { }
