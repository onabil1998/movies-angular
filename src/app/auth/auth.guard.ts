import { Injectable, inject } from "@angular/core";
import { CanActivateFn, Router, UrlTree } from "@angular/router";
import { AuthService } from "./auth.service";
import { Observable, map, take } from "rxjs";

@Injectable()
export class AuthGuard {

    constructor(private authService: AuthService, private router: Router) {}

    canActivate() : boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {
        return this.authService.user.pipe(take(1), map(user => {
           const isNonAuth =  !user;
           if(isNonAuth) {
            return true;
           }
           return this.router.createUrlTree(['/movies']);
        }));
    }
}

export const canActivateTeam: CanActivateFn = () => {
    return inject(AuthGuard).canActivate();
}