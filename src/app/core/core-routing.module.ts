import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesCatalogComponent } from './movies-catalog/movies-catalog.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { nonAuthGuard } from '../auth/non-auth.guard';


const routes: Routes = [
  {path: "", component: MoviesCatalogComponent, canActivate: [nonAuthGuard]},
  {path:':id', component: MovieDetailsComponent, canActivate: [nonAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
