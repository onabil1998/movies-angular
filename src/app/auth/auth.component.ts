import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";

 @Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrl: './auth.component.css'
 })

 export class AuthComponent {
    isLoginMode = false;
    isLoading = false;
    error: string = null;
    signUpSuccessfully = false;

    constructor(private authService: AuthService, private router: Router) {}

    onSwitchMode() {
        this.isLoginMode = !this.isLoginMode;
    }

    onSubmit(form: NgForm) {
        this.error = null;
        this.isLoading = true;
        let email = form.value.email;
        let password = form.value.password;
        this.signUpSuccessfully = false;

        if(this.isLoginMode) {
            this.authService.signIn(email, password).subscribe({
                next: (response) => {
                    this.isLoading = false;
                    this.router.navigate(['/movies'])
                },
                error: (error) => {
                    this.isLoading = false;
                    this.error = error;
                }
            })

        } else {
            this.authService.signUp(email, password).subscribe({
                next: (response) => {
                    this.isLoading = false
                    this.signUpSuccessfully = true;

                },
                error: (error) => {
                    this.error = error;
                    this.isLoading = false
                }
            });
        }
        
      form.reset()
        
    }
 }