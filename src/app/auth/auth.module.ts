import { AuthComponent } from "./auth.component";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AuthGuard } from "./auth.guard";
import { NgModule } from "@angular/core";
import { sharedModule } from "../shared/shared.module";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
    declarations: [
      AuthComponent
    ],
    imports: [
      FormsModule,
      sharedModule,
      MatProgressSpinnerModule,
      RouterModule.forChild([{path: "auth", component: AuthComponent, canActivate: [AuthGuard]}])
    ]
  })
  export class AuthModule { }
  